package patrykpoborca.io.vocabstretch

import android.app.Application
import com.facebook.stetho.InspectorModulesProvider
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp.StethoInterceptor
import com.squareup.okhttp.OkHttpClient
import patrykpoborca.io.vocabstretch.base.ApplicationDelegate
import patrykpoborca.io.vocabstretch.network.WordnikService
import patrykpoborca.io.vocabstretch.manager.VocabularyManager
import retrofit.GsonConverterFactory
import retrofit.Retrofit
import retrofit.RxJavaCallAdapterFactory
import rx.Scheduler
import rx.schedulers.Schedulers

/**
 * Created by patrykpoborca on 1/23/16.
 */
class WordStretchApp : Application(), ApplicationDelegate{
    private var _wordnickService: WordnikService? = null;
    private var _vocabManager: VocabularyManager? = null;

    override fun onCreate() {
        super.onCreate()
        if(Settings.stethoEnabled){
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());
        }
        VocabularyManager.initialize(this);
        _vocabManager = VocabularyManager.getInstance();
        _networkScheduler = Schedulers.io();

    }

    override val wordnickService: WordnikService
        get() {
            val client : OkHttpClient = OkHttpClient();
            client.networkInterceptors().add(StethoInterceptor());

            return _wordnickService ?: Retrofit.Builder()
                    .baseUrl(Settings.baseWordnikUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build()
                    .create(WordnikService::class.java);
        }

    override val vocabManager: VocabularyManager
        get() = _vocabManager!!;

    var _networkScheduler: Scheduler? = null;
    override val networkScheduler: Scheduler
        get() = _networkScheduler!!;
}