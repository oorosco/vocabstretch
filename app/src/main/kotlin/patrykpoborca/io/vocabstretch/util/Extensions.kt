package patrykpoborca.io.vocabstretch.util

import patrykpoborca.io.vocabstretch.views.RootPresenter


/**
 * Created by patrykpoborca on 1/25/16.
 */
fun rx.Subscription.registerSubscription(list: MutableList<rx.Subscription>){
    list.add(this);
}

inline fun <T>List<T>.contains(obj: T, comparisson: (T, T) -> Boolean): Boolean{
    this.forEach {
        iteratedObject ->
        if(comparisson(iteratedObject, obj)){
            return true;
        }
    }
    return false;
}