package patrykpoborca.io.vocabstretch.models

import com.google.gson.annotations.SerializedName

/**
 * Created by patrykpoborca on 1/24/16.
 */
class VocabularyWord {

    //endregion


    @SerializedName("id")
    val id: Int = 0

    @SerializedName("word")
    val word: String? = null

    @SerializedName("attributionText")
    val attribution: String? = null

    //region internal use
    @SerializedName("added_to_vocab")
    var isLearned: Boolean = false
        private set

    @SerializedName("vocabulary_definition")
    var definitions: List<VocabularyDefinitions>? = null

    var isAdded: Boolean
        get() = isLearned
        set(isAdded) {
            this.isLearned = isAdded
        }
}
