package patrykpoborca.io.vocabstretch.models

import com.google.gson.annotations.SerializedName

/**
 * Created by patrykpoborca on 1/25/16.
 */
class VocabularyDefinitions {

    @SerializedName("word")
    val word: String? = null

    @SerializedName("text")
    val definition: String? = null

    @SerializedName("partOfSpeech")
    private val partOfSpeech: String? = null
}
