package patrykpoborca.io.vocabstretch.base

import patrykpoborca.io.vocabstretch.manager.VocabularyManager
import patrykpoborca.io.vocabstretch.network.WordnikService
import rx.Scheduler

/**
 * Created by patrykpoborca on 1/23/16.
 */
interface ApplicationDelegate{
    val wordnickService: WordnikService;
    val vocabManager: VocabularyManager;
    val networkScheduler: Scheduler;
}