package patrykpoborca.io.vocabstretch.base

import android.content.res.Resources
import patrykpoborca.io.vocabstretch.network.WordnikService
import rx.Scheduler
import rx.Subscription
import rx.subjects.BehaviorSubject
import java.util.*

/**
 * Created by patrykpoborca on 1/23/16.
 */
open class BasePresenter<T : BasePresenterView>{

    private var _basePresenterView : T? = null;
    protected  val presenterView: T
        get() = _basePresenterView!!;
    protected val resources : Resources
        get() = _basePresenterView!!._resources;
    protected var subscriptions: MutableList<Subscription> = ArrayList();
    protected val wordnikService: WordnikService
        get() = appDelegate.wordnickService;
    protected val networkScheduler:Scheduler
        get() = appDelegate.networkScheduler;

    private var _appDelegate : ApplicationDelegate? = null;
    protected val appDelegate : ApplicationDelegate
        get() = _appDelegate!!;

    public fun setup(basePresenterView : T, appDelegate : ApplicationDelegate){
        _basePresenterView = basePresenterView;
        _appDelegate = appDelegate;
    }

    open public fun onResume(){
    }

    open public fun onPause(){
        subscriptions.forEach { s -> s.unsubscribe() }
        subscriptions = ArrayList();
    }
}