package patrykpoborca.io.vocabstretch.base

import android.content.res.Resources
import android.support.v7.app.AppCompatActivity

/**
 * Created by patrykpoborca on 1/25/16.
 */
abstract open class PresenterActivity<A : BasePresenterView, B : BasePresenter<A>>: AppCompatActivity(), BasePresenterView{
    override val _resources: Resources
        get() = resources;
    private var _presenter : B? = null;
    private val presenter: B
        get(){
            if(_presenter == null){
                _presenter = presenterClass().newInstance();
                _presenter?.setup(this as A, getApplication() as ApplicationDelegate);
            }
            return _presenter!!;
        }

    override fun onResume() {
        super.onResume()
        presenter.onResume();
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause();
    }
    abstract fun presenterClass() : Class<B>;

}