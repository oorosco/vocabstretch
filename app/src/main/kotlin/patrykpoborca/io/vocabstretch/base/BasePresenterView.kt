package patrykpoborca.io.vocabstretch.base

import android.content.res.Resources
import patrykpoborca.io.vocabstretch.models.VocabularyWord
import rx.Observable

/**
 * Created by patrykpoborca on 1/23/16.
 */
interface BasePresenterView {
    val _resources : Resources;
}
