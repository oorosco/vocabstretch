package patrykpoborca.io.vocabstretch.base

import android.content.res.Resources
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class PresenterFragment<A : BasePresenterView, B : BasePresenter<A>> : Fragment(), BasePresenterView{

    override val _resources: Resources
        get() = resources;
    private var _presenter : B? = null;
    private val presenter: B
        get(){
            if(_presenter == null){
                _presenter = presenterClass().newInstance();
                _presenter?.setup(this as A, activity.application as ApplicationDelegate);
            }
            return _presenter!!;
        }

    override fun onResume() {
        super.onResume()
        presenter.onResume();
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause();
    }

    abstract fun presenterClass() : Class<B>;
}
