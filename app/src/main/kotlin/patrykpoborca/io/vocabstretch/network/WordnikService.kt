package patrykpoborca.io.vocabstretch.network
import patrykpoborca.io.vocabstretch.models.VocabularyDefinitions
import patrykpoborca.io.vocabstretch.models.VocabularyWord
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Headers
import retrofit.http.Path
import retrofit.http.Query
import rx.Observable

/**
 * Created by patrykpoborca on 1/23/16.
 */
interface  WordnikService{

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("words.json/randomWords") fun getRandomWords(@Query("api_key") apiKey: String, @Query("limit") limit: Int): Observable<MutableList<VocabularyWord>>;

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("word.json/{word}/definitions") fun getDefinition(@Path("word") word: String, @Query("api_key") apiKey: String, @Query("limit") limit: Int): Observable<MutableList<VocabularyDefinitions>>;
}