package patrykpoborca.io.vocabstretch.network

import patrykpoborca.io.vocabstretch.models.VocabularyWord
import patrykpoborca.io.vocabstretch.views.RootPresenter
import patrykpoborca.io.vocabstretch.views.RootPresenter.*
import retrofit.Callback
import retrofit.Response
import retrofit.Retrofit

/**
 * Created by patrykpoborca on 1/24/16.
 */
class Attempt : Callback<List<VocabularyWord>> {
    override fun onResponse(response: Response<List<VocabularyWord>>?, retrofit: Retrofit?) {
        throw UnsupportedOperationException()
    }

    override fun onFailure(t: Throwable?) {
        val k = RootPresenter();


        throw UnsupportedOperationException()
    }
}