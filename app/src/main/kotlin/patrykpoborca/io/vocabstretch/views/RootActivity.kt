package patrykpoborca.io.vocabstretch.views

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import patrykpoborca.io.vocabstretch.R
import patrykpoborca.io.vocabstretch.Settings
import patrykpoborca.io.vocabstretch.base.ApplicationDelegate
import patrykpoborca.io.vocabstretch.base.PresenterActivity
import patrykpoborca.io.vocabstretch.manager.VocabularyManager
import patrykpoborca.io.vocabstretch.models.VocabularyWord
import patrykpoborca.io.vocabstretch.views.introduction.IntroductionFragment
import rx.Observable
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject

class RootActivity : PresenterActivity<RootPView, RootPresenter>(), RootPView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_root)
        var transaction = supportFragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, IntroductionFragment());
        transaction.commit();

    }

    fun executeNetwork(){
        (getApplication() as ApplicationDelegate).wordnickService.getRandomWords(Settings.wordnikeApiKey, 50)
                .subscribeOn(Schedulers.io())
                .doOnError { e -> error()  }
                .subscribe({ s ->
                    VocabularyManager.getInstance().vocabWords = s;
                },
                        {error ->
                            error()});


//        .enqueue(object : Callback<List<VocabularyWord>> {
//
//            override fun onResponse(response: Response<List<VocabularyWord>>?, retrofit: Retrofit?) {
//                success();
//            }
//
//            override fun onFailure(t: Throwable) {
//                error();
//            }
//        })
    }

    fun error(){
        Toast.makeText(this, "Call failed", Toast.LENGTH_LONG)
    }

    fun success(){
        Toast.makeText(this, "Call complete", Toast.LENGTH_LONG)
    }

    override fun presenterClass(): Class<RootPresenter> {
        return RootPresenter::class.java;
    }

    override fun pushNextWord(word: VocabularyWord) {

    }

    override fun nextWordStream(): Observable<Void?> {
        return Observable.just(null);
    }
}
