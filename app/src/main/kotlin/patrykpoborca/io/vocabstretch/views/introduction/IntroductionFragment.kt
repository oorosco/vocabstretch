package patrykpoborca.io.vocabstretch.views.introduction

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import patrykpoborca.io.vocabstretch.R

/**
 * Created by patrykpoborca on 1/25/16.
 */
class IntroductionFragment: Fragment(){

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fr_introduction, container, false);
    }
}