package patrykpoborca.io.vocabstretch.views

import android.os.Handler
import android.util.Log
import patrykpoborca.io.vocabstretch.Settings
import patrykpoborca.io.vocabstretch.base.BasePresenter
import patrykpoborca.io.vocabstretch.models.VocabularyDefinitions
import patrykpoborca.io.vocabstretch.models.VocabularyWord
import patrykpoborca.io.vocabstretch.util.contains
import patrykpoborca.io.vocabstretch.util.registerSubscription
import rx.Observable
import rx.Subscription
import rx.observables.ConnectableObservable
import java.util.concurrent.TimeUnit

/**
 * Created by patrykpoborca on 1/25/16.
 */
open class RootPresenter: BasePresenter<RootPView>(){

    var vocabWords: MutableList<VocabularyWord>
        get() = appDelegate.vocabManager.vocabWords
        set(value) {
            appDelegate.vocabManager.vocabWords = value;
        }

    var vocabIndex = 0;

    override fun onResume() {
        super.onResume()
        vocabIndex = 0;
        presenterView.nextWordStream()
            .doOnError{e -> error(e)}
            .delay(5, TimeUnit.SECONDS)
            .subscribe{
                pushVocab()
            }
            .registerSubscription(subscriptions);
    }

    fun pushVocab(){
        presenterView.pushNextWord(vocabWords.get(vocabIndex));
        var undefinedWordCount = 0;
        var learnedWordCount = 0;
        vocabWords.forEach {
            v ->
            if(v.definitions == null){
                undefinedWordCount ++;
            }
            if(v.isLearned){
                learnedWordCount ++;
            }
        }

        val lambda: (VocabularyWord, VocabularyWord) -> Boolean = {one, two -> one.id == two.id};
        if(vocabWords.size - learnedWordCount < 10){
            appDelegate.wordnickService.getRandomWords(Settings.wordnikeApiKey, 50)
                    .subscribe{ words ->
                    vocabWords = words.filter { word -> vocabWords.contains(word, lambda)} as MutableList<VocabularyWord>}
                .registerSubscription(subscriptions)
        }

        var ddelay:Long = 1100;
        var combinedObservable: Observable<MutableList<VocabularyDefinitions>>? = null;
        Log.d("Hello", "undefinedCount = $undefinedWordCount learnedCount = $learnedWordCount");
        Log.d("Hello", "result = " + (undefinedWordCount - learnedWordCount < 10))
        if(learnedWordCount - undefinedWordCount < 10){
            Log.d("Hello", "Entered into if condition");
            vocabWords.filter {
                w -> w.definitions == null
            }
            .forEach {
                w ->
                val current = wordnikService.getDefinition(w.word!!, Settings.wordnikeApiKey, 1)
                if(combinedObservable == null){
                    combinedObservable = current;
                }
                else{
                    concatTwo(combinedObservable!!, current);
                }

                current
                    .subscribeOn(networkScheduler)
                    .doOnError { error -> errorOccured(error) }
                    .subscribe{ definition ->  success(w, definition)}
                    .registerSubscription(subscriptions);
                ddelay += 1100;
            }

            combinedObservable!!.subscribe{ c ->

                vocabWords = vocabWords; //resave to shared preferences
            }.registerSubscription(subscriptions)
        }
        vocabIndex++;
    }

    fun success(w: VocabularyWord, definition: MutableList<VocabularyDefinitions>){
        w.definitions = definition
    }

    private fun errorOccured(error: Throwable?) {
        Log.e("Hello", "", error);
    }

    fun concatTwo(c:Observable<MutableList<VocabularyDefinitions>>, current: Observable<MutableList<VocabularyDefinitions>>): Observable<MutableList<VocabularyDefinitions>>{
        return c.concatWith(current)
    }

    override fun onPause() {
        super.onPause()
    }
}