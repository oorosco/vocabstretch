package patrykpoborca.io.vocabstretch.views

import patrykpoborca.io.vocabstretch.base.BasePresenterView
import patrykpoborca.io.vocabstretch.models.VocabularyWord
import rx.Observable

/**
 * Created by patrykpoborca on 1/25/16.
 */
interface  RootPView: BasePresenterView{

    fun pushNextWord(word: VocabularyWord);

    fun nextWordStream(): Observable<Void?>;
}