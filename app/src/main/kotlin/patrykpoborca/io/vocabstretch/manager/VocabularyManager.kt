package patrykpoborca.io.vocabstretch.manager

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import patrykpoborca.io.vocabstretch.models.VocabularyWord
import java.lang.reflect.Type
import java.util.*

/**
 * Created by patrykpoborca on 1/24/16.
 */
class VocabularyManager{
    private val sharedPreferences: SharedPreferences;
    private var _gson: Gson? = null;
    private val gson: Gson
        get() = _gson ?: Gson();

    private val memCache : HashMap<String, Any> = HashMap();
    private constructor(sharedPreferences: SharedPreferences){
        this.sharedPreferences = sharedPreferences;
    }

    private val vocabWordsKey: String = "vocabWords";


    public var vocabWords: MutableList<VocabularyWord>
            get() {
                val type: Type = object : TypeToken<MutableList<VocabularyWord>>(){}.type;
                val list: MutableList<VocabularyWord>? = extractModel(vocabWordsKey, type);
                return list ?: ArrayList();
            }
            set(value) {
                saveModel(vocabWordsKey, value);
            }



    private fun <T>extractModel(key: String, type: Type) : T?{
        if(memCache.containsKey(key)){
            return memCache.get(key) as T;
        }
        var serializedModel: String? = sharedPreferences.getString(key, null);
        serializedModel?.let {
            s ->
            return gson.fromJson<T>(s, type);
        }
        return null;
    }

    private fun <T>saveModel(key: String, model: T?){
        if(model == null){
            sharedPreferences.edit().remove(key).apply();
            memCache.remove(key)
        }
        else{
            sharedPreferences.edit().putString(key, gson.toJson(model)).apply();
            memCache.put(key, model);
        }
    }

    companion object {
        var vocabManager : VocabularyManager? = null;
        fun getInstance() : VocabularyManager{
            return vocabManager!!;
        }

        fun initialize(context : Context){
            vocabManager = VocabularyManager(context.getSharedPreferences(VocabularyManager.javaClass.canonicalName, Context.MODE_PRIVATE));
        }
    }
}